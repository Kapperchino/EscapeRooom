// Fill out your copyright notice in the Description page of Project Settings.

#include "CloseDoor.h"


// Sets default values for this component's properties
UCloseDoor::UCloseDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UCloseDoor::BeginPlay()
{
	Super::BeginPlay();
	if(!trigger)
		UE_LOG(LogTemp, Error, TEXT("%s missing volume!"), *GetOwner()->GetName());
	Owner = GetOwner();

	// ...

}

void UCloseDoor::OpenDoor()
{
	OnOpenRequest.Broadcast();
}

void UCloseDoor::Close()
{
	OnCloseRequest.Broadcast();
}

float UCloseDoor::GetTotalMassOnPlate()
{
	float mass = 0.;
	if (!trigger)
		return 0.;
	trigger->GetOverlappingActors(OverlappingActors);
	for (const auto*actors : OverlappingActors)
	{
		mass+=actors->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}
	return mass;
}

// Called when the game starts





// Called every frame
void UCloseDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	if (!trigger)
		return;
	if (GetTotalMassOnPlate() > 0)
	{
		OpenDoor();
	}
	else {
		Close();
	}
}

