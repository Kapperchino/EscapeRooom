// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	Controller = GetWorld()->GetFirstPlayerController();
	TraceParams = new FCollisionQueryParams(FName(TEXT("")), false, GetOwner());
	
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("%s missing physics handle!"), *GetOwner()->GetName());
		return;
	}
	input = GetOwner()->FindComponentByClass<UInputComponent>();
	
	if (input)
	{
		input->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		input->BindAction("Grab", IE_Released, this, &UGrabber::Release);
		
	}
	
}

void UGrabber::Grab()
{
	GetFirstPhysicsBodyInReach();
	auto ComponentToGrab = Hit.GetComponent();

	if (ActorHit)
	{
		PhysicsHandle->GrabComponent(ComponentToGrab, NAME_None, GetPlayerLocation(), true);
	}
}

void UGrabber::Release()
{
	PhysicsHandle->ReleaseComponent();
}

void UGrabber::UpdateRay()
{
	Controller->GetPlayerViewPoint(PlayerLocation, PlayerRotate);
	TraceLine = PlayerLocation + PlayerRotate.Vector()*Reach;
}

FVector UGrabber::GetPlayerLocation()
{	
	UpdateRay();
	return PlayerLocation;
}

FVector UGrabber::GetTraceLine()
{
	UpdateRay();
	return TraceLine;
}



const FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{
	
	GetWorld()->LineTraceSingleByObjectType(Hit, GetPlayerLocation(), GetTraceLine(),
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams);

	ActorHit = Hit.GetActor();

	if (ActorHit)
	{
		UE_LOG(LogTemp, Warning, TEXT("Line trace hit: %s"), *ActorHit->GetName());
	}
	return Hit;
}


// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (PhysicsHandle == nullptr)
	{
		return;
	}
	
	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->SetTargetLocation(GetTraceLine());
	}

	// ...
}

