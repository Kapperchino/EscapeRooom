// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/PrimitiveComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

private:
	AActor* Player = nullptr;
	AActor* ActorHit = nullptr;
	APlayerController* Controller = nullptr;
	FVector PlayerLocation;
	FVector TraceLine;
	FRotator PlayerRotate;
	FHitResult Hit;
	FCollisionQueryParams TraceParams;
	
	UPhysicsHandleComponent* PhysicsHandle = nullptr;

	UInputComponent* input = nullptr;

	void Grab();
	void Release();
	void UpdateRay();
	FVector GetPlayerLocation();
	FVector GetTraceLine();
	const FHitResult GetFirstPhysicsBodyInReach();

	UPROPERTY(EditAnywhere)
	float Reach = 100.f;
	

public:	
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
	
};
